#lang scribble/manual

@(require "defs.rkt")

@(under-construction)

@title[#:tag "advanced-topics" #:version apt-version]{Advanced Topics}

@section[#:tag "disk-images"]{Disk Images}

Disk images in @(tb) are stored and distributed in the
@hyperlink["http://www.flux.utah.edu/paper/hibler-atc03"]{Frisbee} disk
image format. They are stored at block level, meaning that, in theory,
any filesystem can be used. In practice, Frisbee's filesystem-aware
compression is used, which causes the image snapshotting and installation
processes to parse the filesystem and skip free blocks; this provides large
performance benefits and space savings. Frisbee has support for filesystems
that are variants of the BSD UFS/FFS, Linux ext, and Windows NTFS formats. The
disk images created by Frisbee are bit-for-bit identical with the original
image, with the caveat that free blocks are skipped and may contain leftover
data from previous users.

Disk images in @(tb) are typically created by starting with one of @(tb)'s
supplied images, customizing the contents, and taking a snapshot of the
resulting disk. The snapshotting process reboots the node, as it boots into an
MFS to ensure a quiescent disk. If you wish to bring in an image from outside
of @(tb) or create a new one from scratch, please
@seclink["getting-help"]{contact us} for help; if this is a common request, we
may add features to make it easier.

@(tb) has default disk image for each node type; after a node is freed by
one experimenter, it is re-loaded with the default image before being released
back into the free pool. As a result, profiles that use the default disk
images typically instantiate faster than those that use custom images, as no
disk loading occurs.

Frisbee loads disk images using a custom multicast protocol, so loading large
numbers of nodes typically does not slow down the instantiation process
much.

Images may be referred to in requests in three ways: by URN, by an unqualified
name, and by URL. URNs refer to a specific image that may be hosted on any of
the @seclink["hardware"]{@(tb)-affilated clusters}. An unqualified name refers
to the version of the image hosted on the cluster on which the experiment is
instantiated. If you have large images that @(tb) cannot store due to space
constraints, you may host them yourself on a webserver and put the URL for
the image into the profile. @(tb) will fetch your image on demand, and cache
it for some period of time for efficient distribution.

Images in @(tb) are versioned, and @(tb) records the provenance of images.
That is, when you create an image by snapshotting a disk that was previously
loaded with another image, @(tb) records which image was used as a base for
the new image, and stores only the blocks that differ between the two. Image
URLs and URNs can contain version numbers, in which case they refer to that
specific version of the image, or they may omit the version number, in which
case they refer to the latest version of the image at the time an experiment
is instantiated.

@section[#:tag "rspecs"]{RSpecs}

The resources (nodes, links, etc.) that define a profile are expressed in the
@hyperlink["http://groups.geni.net/geni/wiki/GENIExperimenter/RSpecs"]{RSpec}
format from the GENI project. In general, RSpec should be thought of as a
sort of ``assembly language''---something it's best not to edit yourself, but
as manipulate with other tools or create as a ``compiled'' target from a
higher-level language.

That said, the tools for manipulating RSpecs are still incomplete. (For a
preview of @(tb)'s plans in this regard, see our section on
@seclink["planned-easier-profiles"]{planned profile creation features}.) So,
there are still some cases in which it is unfortunately useful to look at or
manipulate RSpecs directly.

@(under-construction)
@italic{Still to come: documentation of @(tb)'s extensions to the RSpec format.}



@section[#:tag "public-ip-access"]{Public IP Access}

@(tb) treats publicly-routable IP addresses as an allocatable resource.

By default, all @seclink["physical-machines"]{physical hosts} are given a
public IP address. This IP address is determined by the host, rather than the
experiment. There are two DNS names that point to this public address: a
static one that is the node's permanent hostname (such as
@tt{apt042.apt.emulab.net}), and a dynamic one that is assigned based on the
experiment; this one may look like @tt{<vname>.<exp>.<proj>.apt.emulab.net},
where @tt{<vname>} is the name assigned in the @seclink["rspecs"]{request
RSpec}, @tt{<eid>} is the name assigned to the
@seclink["experiments"]{experiment}, and @tt{proj} is the
@seclink["projects"]{project} that the experiment belongs to. This name is
predictable regardless of the physical nodes assigned.

By default, @seclink["virtual-machines"]{virtual machines} are @italic{not}
given public IP addresses; basic remote access is provided through an @tt{ssh}
server running on a non-standard port, using the physical host's IP address.
This port can be discovered through the @seclink["rspecs"]{manifest} of an
instantiated experiment, or on the ``list view'' of the experiment page.
If a public IP address is required for a virtual machine (for example, to
host a webserver on it), a public address can be requested on a per-VM basis.
If using @seclink["jacks"]{the Jacks GUI}, each VM has a checkbox to request
a public address. If using @seclink["geni-lib"]{python scripts and
@tt{geni-lib}}, setting the @tt{routable_control_ip} property of a node
accomplishes the same effect.
Different clusters will have different numbers of public addresses available
for allocation in this manner.

@subsection[#:tag "dynamic-public-ip"]{Dynamic Public IP Addresses}

In some cases, users would like to create their own virtual machines, and would
like to give them public IP addresses. We also allow profiles to request
a pool of dynamic addresses; VMs brought up by the user can then run DHCP to
be assigned one of these addresses.

Profiles using @seclink["geni-lib"]{python scripts and @tt{geni-lib}} can
request dynamic IP address pools by constructing an @tt{AddressPool} object
(defined in the @tt{geni.rspec.igext} module), as in the following example:

@code-sample["geni-lib-dynamic-ip-pool.py"]

The addresses assigned to the pool are found in the experiment
@seclink["rspecs"]{manifest}.

@section[#:tag "markdown"]{Markdown}

@(tb) supports
@hyperlink["http://daringfireball.net/projects/markdown/"]{Markdown}
in the major text fields in @seclink["rspecs"]{RSpecs}. Markdown is a simple
formatting syntax with a straightforward translation to basic HTML elements
such as headers, lists, and pre-formatted text. You may find this useful in 
the description and instructions attached to your profile.

While editing a profile, you can preview the Markdown rendering of the
Instructions or Description field by double-clicking within the text
box.

@screenshot["markdown-preview.png"]

You will probably find the
@hyperlink["http://daringfireball.net/projects/markdown/"]{Markdown manual} to
be useful.

@section[#:tag "tours"]{Tours}

@italic{This feature under development}

@section[#:tag "introspection"]{Introspection}

@(tb) implements @hyperlink["http://groups.geni.net/geni/wiki/GeniApi"]{the
GENI APIs}, and in particular
@hyperlink["http://groups.geni.net/geni/wiki/GeniGet"]{the @tt{geni-get
command}}.  @tt{geni-get} is a generic means for nodes to query their own
configuration and metadata, and is pre-installed on all facility-provided
disk images.  (If you are supplying your own disk image built from scratch,
you can add the @tt{geni-get} client from
@hyperlink["https://www.emulab.net/downloads/geni-get.tar.gz"]{its
repository}.)

While @tt{geni-get} supports many options, there are three commands most
useful in the @(tb) context.

@subsection[#:tag "geni-get-client-id"]{Client ID}

Invoking @tt{geni-get client_id} will print a single line to standard
output showing the identifier specified in the profile corresponding to
the node on which it is run.  This is a particularly useful feature in
@seclink["geni-lib-example-os-install-scripts"]{execute services}, where
a script might want to vary its behaviour between different nodes.

@subsection[#:tag "geni-get-control_mac"]{Control MAC}

The command @tt{geni-get control_mac} will print the MAC address of
the control interface (as a string of 12 hexadecimal digits with no
punctuation).  In some circumstances this can be a useful means
to determine which interface is attached to the control network, as
OSes are not necessarily consistent in assigning identifiers to
network interfaces.

@subsection[#:tag "geni-get-manifest"]{Manifest}

To retrieve the @seclink["rspecs"]{manifest RSpec} for the instance,
you can use the command @tt{geni-get manifest}.  It will print the
manifest to standard output, including any annotations added during
instantiation.  For instance, this is an appropriate technique to use to
query the allocation of a @seclink["dynamic-public-ip"]{dynamic public
IP address pool}.

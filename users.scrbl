#lang scribble/manual
@(require "defs.rkt")

@title[#:tag "users" #:version apt-version]{@(tb) Users}

@apt-only{

    You may either use @(tb) as a @seclink["guest-users"]{guest} or as a
    @seclink["registered-users"]{registered user}.

    Using @(tb) as a guest is a great way to give it a try; if you find it
    useful and want to start using it for ``real work,'' you should
    @seclink["register"]{sign up for a (free) account}, because a guest account
    (1) won't let you hold your experiments for very long and (2) only allows
    you to use @seclink["virtual-machines"]{virtual machines}, which are not
    ideal for @seclink["repeatable-research"]{reproducing results}, since they
    don't have strong performance isolation from other users.

    @section[#:tag "guest-users"]{Guest Users}

    You may become a guest user simply by entering your email address on
    @(tb)'s @hyperlink[(apturl "instantiate.php")]{``Instantiate an
    Experiment''} page and picking a username. @(tb) will send you an email
    with a verification code - be sure to check your spam folder if you don't
    receive it within a few minutes.

    You'll remain logged in to @(tb) as long as you use the same browser and it
    retains its cookies. If you get logged out for any reason, simply enter the
    same email address and username again, and you'll be sent a new
    verification code.

    Guest users are limited in several ways:

    @itemlist[
     @item{Guests are only allowed to hold experiments for a short period of
         time---a few hours to start with, and they can extend this up to a day}
     @item{Access to some resources (such as bare metal and large VMs) is not
         allowed, meaning that some profiles which require these things are not
         available}
     @item{Experiments held by guest user are very heavily firewalled---no
         outgoing connections are allowed, and almost all incoming traffic is
         blocked}
     @item{Guest users are only allowed to have one active experiment at a time}
     @item{Guest users may not create profiles}
    ]

    If you are going to use @(tb) for much serious work, we encourage you to
    @seclink["register"]{register for an account}.
}

@section[#:tag "registered-users"]{Registered Users}

Registering for an account is @seclink["register"]{quick and easy}. Registering
doesn't cost anything, it's simply for accountability. We just ask that if
you're going to use @(tb) for anything other than light use, you tell us a bit
more about who you are and what you want to use @(tb) for.

Users in @(tb) are grouped into @seclink["projects"]{@italic{projects}}: a
project is a (loosely-defined) group of people working together on some common
goal, whether that be a research project, a class, etc. @(tb) places a lot of
trust on project leaders, including the ability to authorize others to use the
@(tb). We therefore require that project leaders be faculty, senior research
staff, or others who are relatively senior positions.

@clab-only{
    @section[#:tag "geni-users"]{GENI Users}

    If you already have a @hyperlink["http://www.geni.net"]{GENI} account,
    you may use it instead of creating a new CloudLab account. On the login
    page, select the ``GENI User'' button. You will be taken to a page like
    the one below to select where you normally log into your GENI account.

    @screenshot["pick-ma.png"]

    From here, you will be taken to the login page of your GENI federate; for
    example, the login page for the @hyperlink["http://portal.geni.net"]{GENI
    portal} is shown below.

    @screenshot["geni-portal-login.png"]

    After you log in, you will asked to authorize the @(tb) portal to use
    this account on your behalf. If your certificate at your GENI aggregate
    has a passphrase on it, you may be asked to enter that passphrase; if not,
    (as is the case with the GENI portal) you will simply see an ``authorize''
    button as below:

    @screenshot["trusted-signer.png"]

    That's it! When you log in a second time, some of these steps may be
    skipped, as your browser has them cached.

}


@section[#:tag "register"]{Register for an Account}

To get an account on @(tb), you either @seclink["join-project"]{join an existing
project} or @seclink["create-project"]{create a new one}. In general, if you are
a student, you should join a project led by a faculty member with whom
you're working.

If you already have an account on
@hyperlink["http://www.emulab.net/"]{Emulab.net}, you don't need to sign
up for a new account on @(tb)---simply log in with your Emulab username and
password.

@subsection[#:tag "join-project"]{Join an existing project}

@screenshot["join-project.png"]

To join an existing project, simply use the ``Sign Up'' button found on every
@(tb) page. The form will ask you a few basic questions about yourself and the
institution you're affiliated with.

An SSH public key is required; if you're
unfamiliar with creating and using ssh keypairs, we recommend taking a look
at the first few steps in
@hyperlink["https://help.github.com/articles/generating-ssh-keys"]{GitHub's
guide to generating SSH keys}. (Obviously, the steps about how to upload the
keypair into GitHub don't apply to @(tb).)

@margin-note{@(tb) will send you email to confirm your address---watch for it (it
might end up in your spam folder), as your request won't be processed until
you've confirmed your address.}
You'll be asked to enter the project ID for the project you are asking to
join; you should get this from the leader of the project, likely your advisor
or your class instructor. (If they don't already have a project on @(tb), you
can @seclink["create-project"]{ask them to create one}.) The leader of your
project is responsible for approving your account.


@subsection[#:tag "create-project"]{Create a new project}

@screenshot["create-project.png"]

@margin-note{You should only start a new project if you are a faculty member,
senior research staff, or in some other senior position.  Students should ask
their advisor or course instructor to create a new project.}

To start a new project, use the ``Sign Up'' button found on every @(tb) page. In
addition to basic information about yourself, the form will ask you a few
questions about how you intend to use @(tb). The application will be reviewed
by our staff, so please provide enough information for us to understand the
research or educational value of your project. The review process may take
a few days, and you will receive mail informing you of the outcome.

Every person working in your project needs to have
@seclink["join-project"]{their own account}. You get to approve these
additional users yourself (you will receive email when anyone applies to join.)
It is common, for example, for a faculty member to create a project which is
primarily used by his or her students, who are the ones who run experiments. We
still require that the project leader be the faculty member, as we require that
there is someone in a position of authority we can contact if there are
questions about the activities of the project.

Note that projects in @(tb) are publicly-listed: a page that allows users to
see a list of all projects and search through them does not exist yet, but
it will in the future.

@;{

@subsection[#:tag "invite-project"]{Invite others to join your project}

@TODO{Write about this when the feature is finished}

}

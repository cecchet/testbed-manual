#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "getting-help" #:version apt-version]{Getting Help}

The help forum is the main place to ask questions about @(tb), its use, its
default profiles, etc. Users are strongly encouraged to participate in
answering other users' questions. The help forum is handled through Google
Groups, which, by default, sends all messages to the group to your email
address. You may change your settings to receive only digests, or to turn
email off completely.

The forum is searchable, and we recommend doing a search before asking a
question.

The URL for joining or searching the forum is: @url{@forum-url}

@clab-only{If you have any questions that you do not think are suitable for a
    public forum, you may direct them to
    @hyperlink["mailto:support@cloulab.us"]{support@"@"cloudlab.us}}

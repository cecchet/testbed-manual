#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "creating-profiles" #:version apt-version]{Creating Profiles}

@apt-only{
    Using @(tb), you can share your research artifacts with others by creating
    your own profiles.
}

@clab-only{
    In @(tb), a profile captures an entire cloud environment---the software
    needed to run a particular cloud, plus a description of the hardware
    (including network topology) that the cloud software stack should run on.
}

When you create a new profile, you are creating a new
@seclink["rspecs"]{RSpec}, and, usually, creating one or more
@seclink["disk-images"]{disk images} that are referenced by that RSpec.  When
someone uses your profile, they will get their own
@seclink["experiments"]{experiment} that boots up the resources (virtual or
physical) described by the RSpec.


@section[#:tag "creating-from-existing"]{Creating a profile from an existing one}

The easiest way to create a new profile is by cloning an existing one and
customizing it to your needs. The basic steps are:

@itemlist[ #:style 'ordered
    @item{Find an existing profile that's closest to your needs}
    @item{Create an experiment using that profile}
    @item{Log into the node(s) in your experiment to install your software, 
        configure the operating system, etc.}
    @item{``Clone'' the experiment to create a new profile}
]

@subsection[#:tag "profile-creation-preparation"]{Preparation and precautions}

To create profiles, you need to be a @seclink["register"]{registered user}.

Creating a profile can take a while, so we recommend that you
@seclink["experiments"]{extend your experiment} while creating it, and contact
us if you are worried your experiment might expire before you're done creating
your profile. We also strongly recommend testing your profile fully before
terminating the experiment you're creating it from.

@apt-only{
    If you want your profile to be usable by @seclink["guest-users"]{guest
    users}, keep in mind that there are @seclink["guest-users"]{several
    restrictions} placed on them; among these is the fact that they are not
    allowed to make outgoing connections, meaning that the experiment must be
    self-contained. For example, they will not be able to download software or
    datasets from within the experiment---it should all be contained as part of
    the profile.
}

Your home directory is @bold{not} included in the disk image snapshot! You
will need to install your code and data elsewhere in the image. We recommend
@code{/local/}. Keep in mind that others who use your profile are going to have
their own accounts, so make sure that nothing in your image makes assumptions
about the username, home directory, etc. of the user running it.

Be aware the only the contents of disk (not running process, etc.) are stored
as part of the profile, and as part of the creation process, your node(s) will
be rebooted in order to take consistent snapshots of the disk.

For the time being, this process only works for single-node profiles; we will
add support for multi-node profiles in the future.

@subsection{Creating the Profile}

@itemlist[ #:style 'ordered
    @instructionstep["Create an experiment"]{Create an experiment using the
    profile that is most similar to the one you want to build. Usually, this
    will be one of our facility-provided profiles with a generic installation
    of Linux.}

    @instructionstep["Set up the node the way you want it"]{Log into the node
    and install your software, datasets, packages, etc. Note the
    @seclink["profile-creation-preparation"]{caveat above} that it needs to be
    installed somewhere outside of your home directory, and should not be tied
    to your user account.}

    @instructionstep["Clone the experiment to create a new profile"
                     #:screenshot "clone-button.png" ]{
    While you are logged in, the experiment page for your active experiments
    will have a ``clone'' button. Clicking this button will create a new
    profile based on your running experiment.

    Specifically, the button creates a copy of the @seclink["rspecs"]{RSpec}
    used to create the experiment, passes it to the form used to create new
    profiles, and helps you create a @seclink["disk-images"]{disk image} from
    your running experiment.
    }

    @instructionstep["Fill out information for the new profile"]{
    After clicking on the ``clone'' button, you will see a form that
    allows you to view and edit the basic information associated with your
    profile.

    @screenshot["create-profile-form.png"]

    Each profile must be associated with a @seclink["projects"]{project}. If
    you're a member of more than one project, you'll need to select which one
    you want the profile to belong to.

    Make sure to edit the profile's Description and Instructions.

    The ``Description'' is the text that users will see when your profile is
    listed in @(tb), when the user is selecting which profile to use. It is also
    displayed when @seclink["sharing-profiles"]{following a direct link to your
    profile}. It should give the reader a brief description of what they will
    get if they create an experiment with this profile. If the profile is
    associated with a paper or other publication, this is a good place to
    mention that.  @seclink["markdown"]{Markdown} markup, including hyperlinks,
    are allowed in the profile description.

    The ``Instructions'' text is displayed on the experiment page after the
    user has created an experiment using the profile. This is a good place
    to tell them where the code and data can be found, what scripts they
    might want to run, etc. Again, @seclink["markdown"]{Markdown} is allowed.

    The ``Steps'' section allows you to create a @seclink["tours"]{tour} of your
    profile, which is displayed after a user creates an experiment with it.
    This feature is mostly useful if your profile contains more than one
    node, and you wish to explain to the user what the purpose of each node
    is.

    You have the option of making your profile usable to anyone, only
    registered @(tb) users, or members of your project. Regardless of the
    setting you chose here, @(tb) will also give you a
    @seclink["sharing-profiles"]{direct link} that you can use to share your
    profile with others of your choosing.
    }

    @instructionstep["Click ``Create''" #:screenshot "image-creating.png"]{
        When you click the ``Create'' button, your node will be rebooted,
        so that we can take a consistent snapshot of the disk contents. This
        process can take several minutes or longer, depending on the size
        of your disk image. You can watch the progress on this page. When
        the progress bar reaches the ``Ready'' stage, your new profile is
        ready! It will now show up in your ``My Profiles'' list.}

    @instructionstep["Test your profile"]{Before terminating your experiment
    (or letting it expire), we strongly recommend testing out the profile. If
    you elected to make it publicly visible, it will be listed in the profile
    selection dialog on the front page of @url[(apturl)]. If not,
    you can instantiate it from the listing in your ``My Profiles'' page. If
    the profile will be used by guest users, we recommend testing it as one
    yourself: log out, and instantiate it using a different username (you will
    also have to use an alternate email address.}

    @instructionstep["Share your profile"]{Now that your profile is working,
    you can @seclink["sharing-profiles"]{share it} with others by sending
    them direct links, putting links on your webpage or in papers, etc. See
    ``@secref["sharing-profiles"]'' for more details.}
]

@subsection[#:tag "updating-profiles"]{Updating a profile}

@future-work["planned-versioned-profiles"]

You can update the metadata associated with a profile at any time by going to
the ``My Profiles'' page and clicking on the name of the profile to go to the
profile page. On this page, you can edit any of the text fields (Description,
Instructions, etc.), change the permissions, etc.

@margin-note{As with cloning a profile, this snapshot feature currently only
works with single-node profiles.}

If you need to update the contents of the disk image in the profile, simply
create a new experiment from the profile. (You will only see this button on
experiments created from profiles that you own.) Once your experiment is ready,
you will see a ``Snapshot'' button on the experiment page. Log into your node,
get the disk changed the way you want, and click the button.

@screenshot["snapshot-button.png"]

This button kicks off the same image creation process that occurs during
@seclink["creating-from-existing"]{cloning a profile}. Once it's finished, any
new experiments created from the profile will use the new image.

As with creating a new profile, we recommend testing the profile before letting
your experiment expire. If something goes wrong, we do keep one previous image
file for each profile; currently, the only way to get access to this backup
is to @seclink["getting-help"]{contact us}.

@section[#:tag "jacks"]{Creating a profile with a GUI}

@(tb) embeds the Jacks GUI for simple creation of small profiles. Jacks can
be accessed by clicking the ``topology'' button on the profile creation or
editing page. Jacks is designed to be simple, and to ensure that the topologies
drawn can be instantiated on the @seclink["hardware"]{hardware available}.
Thus, after making certain choices (such as picking an operating system image)
you may find that other choices (such as the node type) become limited.

@screenshot["jacks-blank.png"]

Jacks has a ``palette'' on the left side, giving the set of node types 
(such as physical or virtual machines) that are available. Dragging a node
from this palette onto the larger canvas area on the right adds it to
the topology. To create a link between nodes, move the mouse near the first
node, and a small black line will appear. Click and drag to the second node
to complete the link. To create a LAN (multi-endpoint link), create a link
between two nodes, then drag links from other nodes to the small grey box
that appears in the middle of the original link.

@screenshot["jacks-properties.png"]

To edit the properties of a node or link, select it by clicking on its icon
on the canvas. The panel on the left side will be replace by a property
editor that will allow you to to set the @seclink["disk-images"]{disk image}
for the node, set commands to be run when the node boots, etc. To unselect 
the current node or link, and return to the palette on the left, simply
click a blank area of the canvas.

@section[#:tag "geni-lib"]{Describing a profile with python and @tt{geni-lib}}

@margin-note{This feature is currently in @bold{alpha testing} and may change
in the future.}

@tt{geni-lib} is a tool that allows users to generate @seclink["rspecs"]{RSpec} files from Python
code.  An @bold{experimental} feature of @(tb) is the ability to use @tt{geni-lib}
scripts as the definition of a profile, rather then the more primitive
RSpec format. When you supply a @tt{geni-lib} script on the
@seclink["creating-profiles"]{Create Profile} page, your script is uploaded
to the server so that it can be executed in the @tt{geni-lib} environment. This
allows the script to be verified for correctness, and also produces the 
equivalent RSpec representation that you can view if you so desire.

@screenshot["create-geni-lib-empty.png"]

When you provide a @tt{geni-lib} script, you will see a slightly different set
of buttons on the @seclink["creating-profiles"]{Create Profile} page; next
to the ``Source'' button there is an ``XML'' button that will pop up the
RSpec XML for you to look at. The XML is read-only; if you want to change
the profile, you will need to change the python source code that is
displayed when you click on the ``Source'' button. Each time you change the
python source code, the script is uploaded to the server and processed. Be
sure to save your changes if you are 
@seclink["updating-profiles"]{updating an existing profile}.

The following examples demonstrate basic @tt{geni-lib} usage. More information
about @tt{geni-lib} and additional examples, can be found in the
@hyperlink["https://bitbucket.org/barnstorm/geni-lib/src"]{@tt{geni-lib} repository}. Its full documentation is online at
@link["http://geni-lib.readthedocs.org/"]{geni-lib.readthedocs.org}.

@subsection[#:tag "geni-lib-example-single-vm"]{A single XEN VM node}

@code-sample["geni-lib-single-vm.py"]

@subsection[#:tag "geni-lib-example-single-pc"]{A single physical host}

@code-sample["geni-lib-single-pc.py"]

@subsection[#:tag "geni-lib-example-two-vm-lan"]{Two XenVM nodes with a LAN between them}

@code-sample["geni-lib-two-vm-lan.py"]

@subsection[#:tag "geni-lib-example-two-arm-lan"]{Two ARM64 servers in a LAN}

@code-sample["geni-lib-two-arm-lan.py"]

@subsection[#:tag "geni-lib-example-node-ips"]{Set a specific IP address on each node}

@code-sample["geni-lib-node-ips.py"]


@subsection[#:tag "geni-lib-example-os-install-scripts"]{Specify an operating system and set install and execute scripts}

@code-sample["geni-lib-os-install-scripts.py"]


@section[#:tag "creating-from-scratch"]{Creating a profile from scratch}

@future-work["planned-easier-profiles"]

@(tb) profiles are described by
@hyperlink["http://groups.geni.net/geni/wiki/GENIExperimenter/RSpecs"]{GENI
RSpecs}. You can create a profile directly from an RSpec by using the 
``Create Profile'' option from the ``Actions'' menu. Note that you cannot edit
the text fields until you upload an RSpec, as these fields edit (in-browser)
fields in the RSpec.

@section[#:tag "sharing-profiles"]{Sharing Profiles}

@future-work["planned-versioned-profiles"]

If you chose to make your profile publicly visible, it will show up in the main
``Select Profile'' list on @url[(apturl)].  @(tb) also gives you direct links to
your profiles so that you can share them with others, post them on your
website, publish them in papers, etc. The link can be found on the profile's
detail page, which is linked for your ``My Profiles'' page. If you chose to
make your profile accessible to anyone, the link will take the form
@code[(apturl "/p/<project-id>/<profile-id>")]. If you didn't make the profile
public, the URL will have the form @code[(apturl "/p/<UUID>")], where
@code{UUID} is a 128-bit number so that the URL is not guessable. You can still
share this URLs with anyone you want to have access to the profile---for
example, to give it to a collaborator to try out your work before publishing.

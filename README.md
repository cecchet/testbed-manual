# Apt / CloudLab Documentation

This repository contains the documentation for multiple testbed
environments---currently [Apt](https://aptlab.net) and
[CloudLab](https://cloudlab.us). The two have similar user interfaces and
concepts, so both share much documentation in common.

----------

## Building

### Dependencies

 * [Racket](http://download.racket-lang.org) - Make sure the Racket binaries
    (particularly `scribble`) are in your `$PATH`


 * [LaTeX (eg. TeX Live)](https://www.tug.org/texlive/) for building PDF
    copies of the manual. Most Linux distributions have LaTeX in their package
    managers, and there is a
    [package for Windows](https://www.tug.org/texlive/windows.html) and
    [OS X](https://www.tug.org/mactex/)

 * GNU make

### Building

To build HTML pages:

```
    make 
```

To build Apt HTML pages only:

```
    make apt
```

To build CloudLab HTML pages only:

```
    make cloudlab
```

To build PDFs for both testbeds: (warning: can be quite slow) 

```
    make pdf
```

### Viewing

Simply open `apt-manual/index.html` or `cloudlab-manual/index.html` in your
browser. PDFs are placed in the `pdf/` subdirectory.

----------

## Contributing

This manual is written in Scribble, a documentation tool that is distributed
as part of the Racket language. At a basic level, you can think of Scribble as
being somewhat LaTeX-like, with the main difference that the underlying
language is Racket (a language in the Lisp/Scheme family) rather than the TeX
macro system. Scribble is essentially Racket "inside-out"---most text is passed
directly to the output file, with the exception of Scribble commands, which
start with a `@` and can contain Racket code. Scribble is
[very well documented](http://docs.racket-lang.org/scribble/).

### General strategy for contributing

 * Fork the main [apt-manual](https://gitlab.flux.utah.edu/emulab/apt-manual)
   repository, and submit your changes as merge requests. Sign up for an
   account to do so.

 * Do **not** check in build products, such as the `.html` and `.pdf` files

 * Make sure that the `make all` and `make pdf` targets build properly before
   submitting pull requests

 * **Do** include reasonable commit message---they don't have to be long, but they
   should be meaningful. Author name and email address should be set correctly.

 * **Do** read through the existing manual and try to be consistent with the
   existing writing and source code styles.

 * Do **not** make gratuitous whitespace changes, it makes it harder to see the
   changes you've made.

### File/directory structure

Scribble files end in `.scrbl`. Each corresponds to a chapter. All Scribble
files in this manual include `defs.rkt`, which is written directly in Racket.

The toplevel file for the Apt manual is `apt-manual.scrbl`, and the toplevel file
for CloudLab is `cloudlab-manual.scrbl`. Most other `.scrbl` files are included
by these toplevel files. **If you add a new `.scrbl` file, make sure to include
it in both toplevel files** (unless it is only relevant to one of the testbeds).

The `screenshots/` directory contains subdirectories for Apt and CloudLab: when
screenshots are included (via the `@screenshot` command), the proper subdirectory
will be consulted. This is because the two have somewhat different UIs, and
it's confusing to see a screenshot from one in the manual for the other. **If you
add a new screenshot, make sure to include versions for both testbeds.** See more
on taking screenshots below.

The `code-samples/` directory is used for documentation of `geni-lib` scripts.

There are testbed-specific `.css` files for the HTML output of each of the two
manuals.

### Apt vs. CloudLab

Most of the material in this manual applies to either testbed. For the most
part, technical details are the same across both testbeds, unless they refer to
specific hardware. The biggest differences in the manual tend to be in text
related to policy. If you're not sure, just ask.

When referring to the testbed, use the `@(tb)` command, which will expand to
the name of the testbed that the manual is being built for.

For material that applies only to one testbed or the other, you can enclose it
in an `@apt-only{ }` or `@clab-only{ }` block.

For links, use the `@apturl` command, which prepends the URL of appropriate
testbed (*with* trailing slash) to the argument.

### Other custom commands

 * `@screenshot["FOO.png"]`
 
     Include screenshots/<testbed>/FOO.png, sized to the width of the current
     document

 * `@instructionstep["TITLE" #:screenshot "FOO.png"]{ BODY }`

    Intended for use in an `@itemlist`---formats multi-step tutorial content
    in a consistent way. The `#:screenshot` (and its argument) are optional; if
    included, they are placed at the beginning of the step.

 * `@future-work["TAG"]`

    Adds a note in the margin indicating that there is content eslewhere in
    the manual (probably `planned.scrbl`) describing a related feature that
    we are planning to add in the future

 * `@code-sample["FOO.py"]`

    Pulls in `code-samples/FOO.py`; used primarly to document geni-lib scripts.
    If possible, the script should be a fully-working, standalone python program,
    as we intend to start regression testing of the scripts in the world. The
    script should *not* however, contain the `#!` line at the top.

### Taking screenshots

All screenshots should be as "clean" as possible---this means no extraneous tabs,
UI elements such as add-ons, etc. If you use Firefox, one way of achieving this
is to create a new "profile" that has your usual add-ons, etc. disabled. 

The window should be sized such that the relevant UI elements fill it nicely. It
will automatically be re-sized in the documentation to be a constant width. The
screenshot should include the full browser window in order to provide context.

We take screenshots on a Mac, which has the added benefit of automatically adding
a nice drop-shadow around the window. The shortcut is `command-shift-4`, then
press `space`, then click on the window you want to take a shot of.

For tutorial content (for example, the CloudLab tutorial chapter), it can be
helpful to circle or otherwise highlight the relevant element. We ask that you
try to roughly keep the same style as existing screenshots (which are marked up
with Apple's Preview application).

All screenshots should be in `.png` format.

If you find it difficult at all to take screenshots that are consistent with the
rest of the manual, you can ask us to help take them.

#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "cloudlab-tutorial" #:version apt-version]{@(tb) OpenStack Tutorial}

This tutorial will walk you through the process of creating a small cloud on
@(tb) using OpenStack. Your copy of OpenStack will run on bare-metal machines
that are dedicated for your use for the duration of your experiment. You will
have complete administrative access to these machines, meaning that you have
full ability to customize and/or configure your installation of OpenStack.

@section{Objectives}

In the process of taking this tutorial, you will learn to:

@itemlist[
    @item{Log in to @(tb) using a GENI portal account}
    @item{Create your own cloud by using a pre-defined profile}
    @item{Access resources in a cloud that you create}
    @item{Use administrative access to customize your cloud}
    @item{Clean up your cloud when finished}
    @item{Learn where to get more information}
]

@section{Prerequisites}

This tutorial assumes that:

@itemlist[
    @item{You have an existing account on  the
        @link["https://portal.geni.net"]{GENI portal}. (Instructions for
        getting an account can be found
        @link["http://groups.geni.net/geni/wiki/SignMeUp"]{here}.)}
    @item{You have set up an @(ssh) keypair in your GENI portal
        account. (When logged into the GENI portal, you can add a public key
        under ``Profile -> SSH Keys''.)}
    @item{You have an @(ssh) client set up to use that keypair.}
]

@section{Logging In}

The first step is to log in to @(tb); @(tb) is available to all
researchers and educators who work in cloud computing. If you have
an account at one of its federated facilities, like @link["http://geni.net"]{GENI},
then you already have an account at @(tb).

@margin-note{This document assumes that you will log in using an existing
    @seclink["geni-users"]{GENI account}. If you want to use a different
    account, or need to apply for one, see the @seclink["users"]{chapter about
    user accounts}.}

@itemlist[#:style 'ordered

    @instructionstep["Open the CloudLab web interface"
                     #:screenshot "tutorial/front-page.png"]{
        To log in to @(tb) using a GENI account, start by visiting
        @url{https://www.cloudlab.us/} in your browser and using the ``Log In''
        button in the upper right.
    }

    @instructionstep["Click the \"GENI User\" button"
                     #:screenshot "tutorial/login-page.png"]{
        On the login page that appears, you will use the ``GENI User'' button.
        This will start the GENI authorization tool, which lets you connect
        your GENI account with @(tb). 
    }

    @instructionstep["Select the GENI portal"
                     #:screenshot "tutorial/authorization-tool.png"]{
        You will be asked which facility your account is with. Select the GENI
        icon, which will take you to the GENI portal. There are several other
        facilities that are federated with @(tb), which can be found in the
        drop-down list. 
    }

    @instructionstep["Log into the GENI portal"
                     #:screenshot "tutorial/geni-login.png"]{
        You will be taken to the GENI portal, and will need to select the
        institution that is your identity provider. Usually, this is the
        university you are affiliated with. If your university is not in
        the list, you may need to log in using the ``GENI Project Office''
        identity provider. If you have ever logged in to the GENI portal
        before, your identity provider should be pre-selected; if you
        are not familiar with this login page, there is a good chance that
        you don't have a GENI account and need to 
        @link["http://groups.geni.net/geni/wiki/SignMeUp"]{apply for one}.
    }

    @instructionstep["Log into your institution"
                     #:screenshot "tutorial/utah-login.png"]{
        This will bring you to your institution's standard login page, which you
        likely use to access many on-campus services. (Yours will look different
        from this one.)
    }

    @instructionstep["Authorize CloudLab to use your GENI account"]{
        @margin-note{What's happening in this step is that your browser
        uses your GENI user certificate (which it obtained from the GENI
        portal) to cryptographically sign a
        @link["http://groups.geni.net/geni/wiki/GAPI_AM_API_DRAFT/SpeaksFor"]{``speaks-for'' credential }
        that authorizes the @(tb) portal to make
        @link["http://groups.geni.net/geni/wiki/GeniApi"]{GENI API calls}
        on your behalf.}
        Click the ``Authorize'' button: this will create a signed statement
        authorizing the @(tb) portal to speak on your behalf. This
        authorization is time-limited (see ``Show Advanced'' for the details),
        and all actions the @(tb) portal takes on your behalf are clearly
        traceable.

        @screenshot["tutorial/authorize.png"]

    }


    @instructionstep["Set up an SSH keypair in the GENI portal"
                     #:screenshot "tutorial/portal-keypair.png"]{
        Though not strictly required, many parts of this tutorial will
        work better if you have an @(ssh) public key associated with your
        GENI Portal account. To upload one, visit
        @link["http://portal.geni.net"]{portal.geni.net} and to the
        ``Profile'' page, ``SSH Keys'' tab.

        If you are not familiar with @(ssh) keys, you may skip this step. You
        may find
        @link["https://help.github.com/articles/generating-ssh-keys/"]{GitHub's
        @(ssh) key page} helpful in learning how to generate and use them.
    }
]


@section{Building Your Own OpenStack Cloud}


Once you have logged in to @(tb), you will ``instantiate'' a @seclink["profiles"]{``profile''}
to create an @seclink["experiments"]{experiment}. (An experiment in @(tb) is similar
to a @link["http://groups.geni.net/geni/wiki/GENIConcepts#Slice"]{``slice''} in
GENI.) Profiles are @(tb)'s way of packaging up configurations and experiments
so that they can be shared with others. Each experiment is separate:
the experiment that you create for this tutorial will be an instance of a profile provided by
the facility, but running on resources that are dedicated to you, which you
have complete control over. This profile uses local disk space on the nodes, so
anything you store there will be lost when the experiment terminates.

@margin-note{The OpenStack cloud we will build in this tutorial is very small, but @(tb)
has @seclink["hardware"]{large clusters} that can be used for larger-scale
experiments.}

For this tutorial, we will use a basic profile that brings up a small OpenStack
cloud. The @(tb) staff have built this profile by capturing
@seclink["disk-images"]{disk images} of a partially-completed OpenStack installation
and scripting the remainder of the install (customizing it for the specific
machines that will get allocated, the user that created it, etc.)
See this manual's @seclink["profiles"]{section on profiles} for more
information about how they work.


@itemlist[#:style 'ordered
    @instructionstep["Select a profile"]{

        @screenshot["tutorial/start-experiment.png"]

        @margin-note{After logging in, you will be taken to the
        ``@link["https://www.cloudlab.us/instantiate.php"]{Start an
        Experiment}'' page automatically if you do not have any current,
        running experiments. You can get back to this page at any time by
        selecting the ``Start Experiment'' link from the ``Actions'' menu.}

        The ``Start an Experiment'' page is where you will select a profile
        to instantiate. We will use the @bold{Tutorial-OpenStack} profile; if
        it is not selected, follow
        @link["https://cloudlab.us/p/CloudLab/Tutorial-OpenStack"]{this link}
        or click the ``Change Profile'' button, and select
        ``Tutorial-OpenStack'' from the list on the left.

        Once you have the correct profile selected, click ``Next''

        @screenshot["tutorial/click-next.png"]
    }

    @instructionstep["Select a cluster"
                     #:screenshot "tutorial/pick-cluster.png"]{
        @(tb) has multiple clusters available to it. Some profiles can run
        on any cluster, some can only run on specific ones due to specific hardware
        constraints, etc.

        @bold{Note:} If you are at an in-person tutorial, the instructor will
        tell you which cluster to select. Otherwise, you may select any
        cluster.

        @margin-note{The ``Check Cluster Status'' link opens a page (in a new
        tab) showing the current utilization of all @(tb) clusters---this can
        be useful for finding a cluster that has enough free machines for your
        experiment.}

    }

    @instructionstep["Click Finish!"
                     #:screenshot "tutorial/click-create.png"]{
        When you click the ``finish'' button, @(tb) will start
        provisioning the resources that you requested on the cluster that
        you selected.

        @margin-note{You may optionally give your experiment a name---this
        can be useful if you have many experiments running at once.}

    }

    @instructionstep["CloudLab instantiates your profile"]{
        @(tb) will take a few minutes to bring up your copy of OpenStack, as
        many things happen at this stage, including selecting suitable
        hardware, loading disk images on local storage, booting bare-metal
        machines, re-configuring the network topology, etc. While this is
        happening, you will see this status page:

        @screenshot["tutorial/status-waiting.png"]
        
        @margin-note{Provisioning is done using the 
        @link["http://groups.geni.net/geni/wiki/GeniApi"]{GENI APIs}; it
        is possible for advanced users to bypass the @(tb) portal and
        call these provisioning APIs from their own code. A good way to
        do this is to use the @link["https://geni-lib.readthedocs.org"]{@tt{geni-lib} library for Python.}}


        As soon as a set of resources have been assigned to you, you will see
        details about them at the bottom of the page (though you will not be
        able to log in until they have gone through the process of imaging and
        booting.) While you are waiting for your resources to become available,
        you may want to have a look at the
        @link["http://docs.cloudlab.us"]{@(tb)
        user manual}, or use the ``Sliver'' button to watch the logs of the
        resources (``slivers'') being provisioned and booting.
    }

    @instructionstep["Your cloud is ready!"
                     #:screenshot "tutorial/status-ready.png"]{
         When the web interface reports the state as ``Ready'', your cloud
         is provisioned, and you can proceed to the next section.

         @bold{Important:} A ``Ready'' status indicates that resources are
         provisioned and booted; this particular profile runs scripts to
         complete the OpenStack setup, and it will be a few more minutes before
         OpenStack is fully ready to log in and create virtual machine
         instances. For now, don't attempt to log in to OpenStack, we will
         explore the CloudLab experiment first.

    }

]

@section{Exploring Your Experiment}

Now that your experiment is ready, take a few minutes to look at various parts
of the @(tb) status page to help you understand what resources you've got and what
you can do with them.

@subsection{Experiment Status}

The panel at the top of the page shows the status of your experiment---you can
see which profile it was launched with, when it will expire, etc. The
buttons in this area let you make a copy of the profile (so that you can
@seclink["creating-profiles"]{customize it}), ask to hold on to the resources
for longer, or release them immediately.

@screenshot["tutorial/experiment-status.png"]

Note that the default lifetime for experiments on @(tb) is less than a day;
after this time, the resources will be reclaimed and their disk contents will
be lost. If you need to use them for longer, you can use the ``Extend'' button
and provide a description of why they are needed. Longer extensions require
higher levels of approval from @(tb) staff. You might also consider
@seclink["creating-profiles"]{creating a profile} of your own if you might need
to run a customized environment multiple times or want to share it with others.

You can click the title of the panel to expand or collapse it.


@subsection{Profile Instructions}

Profiles may contain written instructions for their use. Clicking on the title
of the ``Profile Instructions'' panel will expand (or collapse) it; in this
case, the instructions provide a link to the administrative interface of
OpenStack, and give you passwords to use to log in. (Don't log into OpenStack
yet---for now, let's keep exploring the @(tb) interface.)

@screenshot["tutorial/experiment-instructions.png"]

@subsection{Topology View}

At the bottom of the page, you can see the topology of your experiment. This
profile has three nodes connected by a 10 Gigabit LAN, which is represented by
a gray box in the middle of the topology. The names given for each node are
the names assigned as part of the profile; this way, every time you instantiate
a profile, you can refer to the nodes using the same names, regardless of which
physical hardware was assigned to them. The green boxes around each node
indicate that they are up; click the ``Refresh Status'' button to initiate a
fresh check.

@screenshot["tutorial/topology-view.png"]

It is important to note that every node in @(tb) has at least @italic{two}
network interfaces: one ``control network'' that carries public IP
connectivity, and one ``experiment network'' that is isolated from the Internet
and all other experiments. It is the experiment net that is shown in this
topology view.  You will use the control network to @(ssh) into your nodes,
interact with their web interfaces, etc. This separation gives you more
freedom and control in the private experiment network, and sets up a clean
environment for @seclink["repeatable-research"]{repeatable research}.


@subsection[#:tag "tutorial-list-view"]{List View}

The list view tab shows similar information to the topology view, but in a
different format. It shows the identities of the nodes you have been
assigned, and the full @(ssh) command lines to connect to them. In some
browsers (those that support the @tt{ssh://} URL scheme), you can click on the
SSH commands to automatically open a new session. On others, you may need to
cut and paste this command into a terminal window. Note that only public-key
authentication is supported, and you must have set up an @(ssh) keypair on your
account @bold{before} starting the experiment in order for authentication to
work.

@screenshot["tutorial/experiment-list.png"]

@subsection{Manifest View}

The final default tab shows a
@link["http://groups.geni.net/geni/wiki/GENIExperimenter/RSpecs#ManifestRSpec"]{manifest} detailing the hardware that has been assigned to you. This is the
@seclink["rspecs"]{``request'' RSpec} that is used to define the profile,
annotated with details of the hardware that was chosen to instantiate your
request. This information is available on the nodes themselves using the
@link["http://groups.geni.net/geni/wiki/GeniGet"]{@tt{geni-get}} command, 
enabling you to do rich scripting that is fully aware of both the requested
topology and assigned resources.

@margin-note{Most of the information displayed on the @(tb) status page comes
directly from this manifest; it is parsed and laid out in-browser.}

@screenshot["tutorial/experiment-manifest.png"]

@subsection[#:tag "tutorial-actions"]{Actions}

In both the topology and list views, you have access to several actions that
you may take on individual nodes. In the topology view, click on the node to
access this menu; in the list view, it is accessed through the icon in the
``Actions'' column. Available actions include rebooting (power cycling) a node,
and re-loading it with a fresh copy of its disk image (destroying all data on
the node).  While nodes are in the process of rebooting or re-imaging, they
will turn yellow in the topology view. When they have completed, they will
become green again. The @seclink["tutorial-web-shell"]{shell} and
@seclink["tutorial-console"]{console} actions are described in more detail
below.

@screenshot["tutorial/experiment-actions.png"]

@subsection[#:tag "tutorial-web-shell"]{Web-based Shell}

@(tb) provides a browser-based shell for logging into your nodes, which is
accessed through the action menu described above. While this shell is
functional, it is most suited to light, quick tasks; if you are going to do
serious work, on your nodes, we recommend using a standard terminal
and @(ssh) program.

This shell can be used even if you did not establish an @(ssh) keypair with
your account.

Two things of note:

@itemlist[
    @item{Your browser may require you to click in the shell window before
        it gets focus.}

    @item{Depending on your operating system and browser, cutting and pasting into
        the window may not work. If keyboard-based pasting does not
        work, try right-clicking to paste.}
]

@screenshot["tutorial/experiment-shell.png"]

@subsection[#:tag "tutorial-console"]{Serial Console}

@(tb) provides serial console access for all nodes, which can be used
in the event that normal IP or @(ssh) access gets intentionally or
unintentionally broken. Like the browser-based shell, it is launched through
the access menu, and the same caveats listed above apply as well. In addition:

@itemlist[
    @item{If you look at the console for a node that is already booted,
    the console may be blank due to a lack of activity; press enter
    several times to get a fresh login prompt.}

    @item{If you need to log in, do so as @bold{root}; your normal user account
    does not allow password login. There is a link above the console window
    that reveals the randomly-generated root password for your node. Note that
    this password changes frequently for security reasons.}
]

@screenshot["tutorial/experiment-console.png"]

@section[#:tag "tutorial-instances"]{Bringing up Instances in OpenStack}

Now that you have your own copy of OpenStack running, you can use it just like
you would any other OpenStack cloud, with the important property that you have
full @tt{root} access to every machine in the cloud and can modify them however
you'd like. In this part of the tutorial, we'll go through the process of
bringing up a new VM instance in your cloud.

@margin-note{We'll be doing all of the work in this section using the Horizon
web GUI for OpenStack, but you could also @(ssh) into the machines directly and use the
command line interfaces or other APIs as well.}

@itemlist[#:style 'ordered

    @instructionstep["Check to see if OpenStack is ready to log in"]{

        As mentioned earlier, this profile runs several scripts to complete
        the installation of OpenStack. These scripts do things such as
        finalize package installation, customize the installation for the 
        specific set of hardware assigned to your experiment, import cloud
        images, and bring up the hypervisors on the compute node(s).

        If exploring the @(tb) experiment took you more than ten minutes, these
        scripts are probably done. You can be sure by checking:

         @itemlist[
            @item{The setup script will attempt to send mail to the addresses
                associated with your GENI portal account when it is finished.
                We find, however, that this mail often gets caught by spam
                filters.}

            @item{You can watch the setup log by logging into the @tt{ctl}
                (controller)
                node via @(ssh) (either a traditional @(ssh) client, or the
                @seclink["tutorial-web-shell"]{web-based shell} described
                above), and running
                @tt{sudo tail -F /root/setup/setup-controller.log} . When
                this process is finished, the final message will be something
                like @tt{Your OpenStack instance has completed setup!}
            }

         ]

         If you continue without verifying that the setup scripts are
         complete, be aware that you may see temporary errors from the
         OpenStack web interface. These errors, and the method for dealing with
         them, are generally noted in the text below.
    }

    @instructionstep["Visit the OpenStack Horizon web interface"
                     #:screenshot "tutorial/experiment-instructions.png"]{
        On the status page for your experiment, in the ``Instructions''
        panel (click to expand it if it's collapsed), you'll find a link
        to the web interface running on the @tt{ctl} node. Open this
        link (we recommend opening it in a new tab, since you will still
        need information from the @(tb) web interface). 
    }

    @instructionstep["Log in to the OpenStack web interface"
                     #:screenshot "tutorial/os-login.png"]{
        Log in using the username @tt{admin} and the password shown in 
        the instructions for the profile.

        @bold{Important:} if the web interface rejects your password, wait
        a few minutes and try again. If it gives you another type of error,
        you may need to wait a minute and reload the page to get login
        working properly.
    }

    @instructionstep["Launch a new VM instance"
                     #:screenshot "tutorial/os-launch.png"]{
        Click the ``Launch Instance'' button on the ``Instances'' page of the
        web interface.
    }

    @instructionstep["Set Basic Settings For the Instance"]{

        There a few settings you will need to make in order to launch your
        instance:

        @screenshot["tutorial/os-launch-basic.png"]

        @itemlist[
            @item{Pick any ``Instance Name'' you wish}

            @item{Set the ``Flavor'' to @tt{m1.small}---the disk for the
                default @tt{m1.tiny} instance is too small for the image we
                will be using, and since we have only one compute node, we
                want to avoid using up too many of its resources.}

            @item{For the ``Instance Boot Source'', select ``Boot from
                image''}

            @item{For the ``Image Name'', select ``trusty-server''}
        ]

        @bold{Important:} If you do not see any available images,
        the image import script may not have finished yet; wait a few
        minutes, reload the page, and try again. 
    }

    @instructionstep["Set an SSH Keypair"
                     #:screenshot "tutorial/os-launch-key.png"]{
        On the ``Access & Security'' tab, you will add an @(ssh) keypair
        to log into your node. If you configured an @(ssh) key in your GENI
        account, you should find it as one of the options in this list. If
        not, you can add a new keypair with the red button to the right of the
        list. Alternately, you
        can skip this step and use a password for login later.
    }

    @instructionstep["Add a Network to the Instance"
                     #:screenshot "tutorial/os-launch-net.png"]{
        In order to be able to access your instance, you will need to give
        it a network. On the ``Networking'' tab, add the @tt{tun-data-net}
        to the list of selected networks by clicking the ``+'' button or
        dragging it up to the blue region. This will set up an internal
        tunneled connection within your cloud; later, we will assign a public
        IP address to it.

        @margin-note{The @tt{tun-data-net} consists of EGRE tunnels on the @(tb)
        experiment network.}

        @bold{Important:} If you are missing the Networking tab, you may have
        logged into the OpenStack web interface before all services were
        started.  Unfortunately, reloading does not solve this, you will need
        to log out and back in.
    }
    
    @instructionstep["Launch, and Wait For Your Instance to Boot"
                     #:screenshot "tutorial/os-launch-finish.png"]{
        Click the ``Launch'' button, and wait for the status on the
        instances page to show up as ``Active''. 
    }

    @instructionstep["Add a Public IP Address"]{

        @screenshot["tutorial/os-launch-associate.png"]

        At this point, your instance is up, but it has no connection to the
        public Internet. From the menu on the right, select ``Associate
        Floating IP''.

        @margin-note{Profiles may request to have public IP addresses allocated
        to them; this profile requests four (two of which are used by OpenStack
        itself.)}

        On the following screen, you will need to:

        @itemlist[#:style 'ordered

            @item{Press the red button to set up a new IP address}

            @item{Click the ``Allocate IP'' button---this allocates a new
            address for you from the public pool available to your cloud.}

            @item{Click the ``Associate'' button---this associates the
            newly-allocated address with this instance.}
        ]

        @margin-note{The public address is tunneled by the @tt{nm} (network manager) node
        from the control network to the experiment network.}

        You will now see your instance's public address on the ``Instances''
        page, and should be able to ping this address from your laptop or
        desktop.

        @screenshot["tutorial/os-instance-publicip.png"]

    }

    @instructionstep["Log in to Your Instance"]{
        You can @(ssh) to this IP address.  If you provided a public key
        earlier, use the username @bold{ubuntu} and your private @(ssh) key. If
        you did not set up an @(ssh) key, use the username @bold{root} and the
        password shown in the profile instructions. Run a few commands to check
        out the VM you have launched.
    }
]

@section{Administering OpenStack}

Now that you've done some basic tasks in OpenStack, we'll do a few things
that you would not be able to do as a user of someone else's OpenStack
installation. These just scratch the surface---you can upgrade, downgrade,
modify or replace any piece of your own copy of OpenStack.

@subsection{Log Into The Control Nodes}

If you want to watch what's going on with your copy of OpenStack, you can use
@(ssh) to log into any of the hosts as described above in the
@secref["tutorial-list-view"] or @secref["tutorial-web-shell"] sections. Don't
be shy about viewing or modifying things; no one else is using your cloud,
and if you break this one, you can easily get another.

Some things to try:

@itemlist[
    @item{Run @tt{ps -ef} on the @tt{ctl} to see the list of OpenStack
        services running}
    @item{Run @tt{ifconfig} on the @tt{nm} node to see the various
        bridges and tunnels that have been brought to support the networking in
        your cloud}
    @item{Run @tt{sudo virsh list @literal{--}all} on @tt{cp1} to see the VMs that
        are running}
]

@;{

@subsection{Create a New Network}

You can define a new network for instances in your cloud to connect to:

@itemlist[#:style 'ordered
    @instructionstep["Start the ``Create Network'' wizard"
                     #:screenshot "tutorial/os-networks.png"]{
        One the ``Project -> Networks'' page on the OpenStack Horizon
        interface, click ``Create Network.''
    }

    @instructionstep["Give your network a name"
                     #:screenshot "tutorial/os-network-name.png"]{
        Any name will do
    }

    @instructionstep["Create a subnet for your network"
                     #:screenshot "tutorial/os-network-subnet.png"]{
        Give the subnet any name you wish. In the ``Network Address'' field,
        put in a subnet definition (address/netmask). @tt{192.0.2.0/24} (the
        RFC 5737 TEST-NET-1) works well.

        @bold{Important:} This profile already uses all of the usual private
        (RFC 1918) address ranges (@tt{10.0.0.0/8}, @tt{192.168.0.0./16}, and
        @tt{172.16.0.0/12}) for other purposes, so you should not use them
        here.

        Leave the gateway information empty.
    }

    @instructionstep["Enable DHCP"
                     #:screenshot "tutorial/os-network-dhcp.png"]{

        On the final screen, make sure DHCP is enabled, and click the
        ``Create'' button.
    }

    @instructionstep["Use your new network"]{
        
        Follow the instructions from @secref["tutorial-instances"] to bring
        up two new instances. During the launch process, when you get to
        step of adding networks to your new instances, this time you will
        connect them to two networks: connect your instances to 
        @tt{tun-data-net} as before, but also connect them to the new network
        that you just created.

        @bold{Important:} This profile only has two available floating IP
        addresses. If one of them is still associated with your earlier
        instance, only one of these new instances will be able to have a public
        IP address.

        Log in to one of your instances: you should be able to see this new
        network interface with @tt{ifconfig}, and to @tt{ping} the other node
        across it. You can find the addresses that were assigned to each node
        on the ``Instances'' page of the OpenStack interface.
    }

]

}

@subsection{Reboot the Compute Node}

Since you have this cloud to yourself, you can do things like reboot or
re-image nodes whenever you wish. We'll reboot the @tt{cp1} (compute) node and
watch it through the serial console.

@itemlist[#:style 'ordered
    @instructionstep["Open the serial console for cp1"]{
        On your experiment's @(tb) status page, use the action menu as
        described in @secref["tutorial-actions"] to launch the console.
        Remember that you may have to click to focus the console window and hit
        enter a few times to see activity on the console.
    }
    
    @instructionstep["Reboot the node"]{
        On your experiment's @(tb) status page, use the action menu as
        described in @secref["tutorial-actions"] to reboot @tt{cp1}. Note
        that in the topology display, the box around @tt{cp1} will turn
        yellow while it is rebooting, then green again when it has booted.
    }

    @instructionstep["Watch the node boot on the console"
                     #:screenshot "tutorial/reboot-console.png"]{
        Switch back to the console tab you opened earlier, and you
        should see the node starting its reboot process.
    }

    @instructionstep["Check the node status in OpenStack"]{

        You can also watch the node's status from the OpenStack Horizon
        web interface. In Horizon, select ``Hypervisors'' under the ``Admin''
        menu, and switch to the ``Compute Host'' tab.

        @screenshot["tutorial/os-node-down.png"]

        @bold{Note:} This display can take a few minutes to notice that the
        node has gone down, and to notice when it comes back up. Your instances
        will not come back up automatically; you can bring them up with a
        ``Hard Reboot'' from the ``Admin -> System ->Instances'' page.
    }
]

@section{Terminating the Experiment}

Resources that you hold in @(tb) are real, physical machines and are
therefore limited and in high demand. When you are done, you should release
them for use by other experimenters. Do this via the ``Terminate'' button on
the @(tb) experiment status page.

@screenshot["tutorial/status-terminate.png"]

@bold{Note:} When you terminate an experiment, all data on the nodes is lost,
so make sure to copy off any data you may need before terminating.

If you were doing a real experiment, you might need to hold onto the nodes for
longer than the default expiration time. You would request more time by using
the ``Extend'' button the on the status page. You would need to provide a
written justification for holding onto your resources for a longer period of
time.

@section{Taking Next Steps}

Now that you've got a feel for for what @(tb) can do, there are several
things you might try next:

@itemlist[
    @item{@seclink["create-project"]{Create a new project} to continue working
    on @(tb) past this tutorial}
    @item{Try out some profiles other than OpenStack (use the ``Change
        Profile'' button on the ``Start Experiment'' page)}
    @item{Read more about the @seclink["basic-concepts"]{basic concepts} in
        @(tb)}
    @item{Try out different @seclink["hardware"]{hardware}}
    @item{Learn how to @seclink["creating-profiles"]{make your own profiles}}

]

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg

# Create the Portal context.
pc = portal.Context()
 
# Create a Request object to start building the RSpec.
rspec = pg.Request()
 
# Create a raw PC and add it to the RSpec.
node = pg.RawPC("node")
rspec.addResource(node)
 
# Print the RSpec to the enclosing page.
pc.printRequestRSpec(rspec)

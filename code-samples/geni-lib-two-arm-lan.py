import geni.portal as portal
import geni.rspec.pg as pg 

pc = portal.Context()
rspec = pg.Request()

# Create two raw "PC" nodes
node1 = pg.RawPC("node1")
node2 = pg.RawPC("node2")

# Set each of the two to specifically request "m400" nodes, which in CloudLab, are ARM
node1.hardware_type = "m400"
node2.hardware_type = "m400"

# Create interfaces for each node.
iface1 = node1.addInterface("if1")
iface2 = node2.addInterface("if2")

rspec.addResource(node1)
rspec.addResource(node2)

# Create a link with the type of LAN.
link = pg.LAN("lan")

# Add both node interfaces to the link.
link.addInterface(iface1)
link.addInterface(iface2)

# Add the link to the RSpec.
rspec.addResource(link)

pc.printRequestRSpec(rspec)

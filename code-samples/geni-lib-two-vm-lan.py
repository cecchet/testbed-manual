import geni.portal as portal
import geni.rspec.pg as pg 

pc = portal.Context()
rspec = pg.Request()

# Create a XenVM nodes.
node1 = pg.XenVM("node1")
node2 = pg.XenVM("node2")

# Create interfaces for each node.
iface1 = node1.addInterface("if1")
iface2 = node2.addInterface("if2")

rspec.addResource(node1)
rspec.addResource(node2)

# Create a link with the type of LAN.
link = pg.LAN("lan")

# Add both node interfaces to the link.
link.addInterface(iface1)
link.addInterface(iface2)

# Add the link to the RSpec.
rspec.addResource(link)

pc.printRequestRSpec(rspec)
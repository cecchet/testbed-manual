import geni.portal as portal
import geni.rspec.pg as pg 

pc = portal.Context()
rspec = pg.Request()

node1 = pg.XenVM("node1")

# Specify the URL for the disk image
node1.disk_image = "<URL to disk image>"

# Install and execute scripts on the VM
node1.addService(pg.Install(url="<URL to the tarball file>", path="local"))
node1.addService(pg.Execute(shell="bash", command="<Path to executable>"))

rspec.addResource(node1)
pc.printRequestRSpec(rspec)
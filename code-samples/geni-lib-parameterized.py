import geni.portal as portal
import geni.rspec.pg as RSpec

# The legal types of nodes - note that the first string in each pair is the
# string that will get put in the RSpec, and the second is the one that will
# get shown to the user. If there is no need to show the user a different string,
# each element of the array can be a simple string rather than a tuple.
hw_types = [("m400", "ARM64"), ("dl360", "x86-64")]

pc = portal.Context()

# Define parameters - see this page for API documentation for the
# defineParameter() call:
#    http://geni-lib.readthedocs.org/en/latest/api/geniportal.html#geni.portal.Context.defineParameter
pc.defineParameter("N", "Number of nodes",
                   portal.ParameterType.INTEGER, 5)
pc.defineParameter("hwtype", "Hardware type",
                   portal.ParameterType.NODETYPE, "m400", hw_types)
pc.defineParameter("lan",  "Put all nodes in a LAN",
                   portal.ParameterType.BOOLEAN, False)

# Get values for the parameters - since a default is required by defineParameter,
# the params object is guaranteed to contain a vale for every parameter
params = pc.bindParameters()

rspec = RSpec.Request()

# Create a LAN iff one was requested
if (params.lan):
    lan = RSpec.LAN()
    rspec.addResource(lan)

# Loop through the number of nodes requested, settting up each and adding it
# to the RSpec
for i in range(1, params.N+1):
  node = RSpec.RawPC("node%d" % i)
  node.hardware_type = params.hwtype
  rspec.addResource(node)

  # Only create an interface if we are putting all nodes into a LAN
  if params.lan:
    iface = node.addInterface("eth0")
    lan.addInterface(iface)

pc.printRequestRSpec(rspec)

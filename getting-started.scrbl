#lang scribble/manual

@(require "defs.rkt")

@title[#:tag "getting-started" #:version apt-version]{Getting Started}

This chapter will walk you through a simple experiment on @(tb) and
introduce you to some of its @seclink["basic-concepts"]{basic concepts}.

Start by pointing your browser at @url[(apturl)].

@itemlist[#:style 'ordered
  @apt-only{
      @instructionstep["Enter your email address and pick a username"
                       #:screenshot "instantiate-empty.png"]{
          If you don't have an account, or are not logged in, you'll be using
          @(tb) as a @seclink["guest-users"]{guest user}. Guest users have some
          @seclink["guest-users"]{restrictions} placed on them, but this is a
          quick way to get started. Enter your email address and pick a
          username. This username will be the one you'll use to log into your
          nodes (eg. via @(ssh)), and if you @seclink["register"]{sign up} for
          an account on @(tb) later, it will become your @(tb) username as
          well.

          You may leave the @(ssh) key portion of the form blank---if you do,
          the only way to log into your nodes will be through a browser-based
          terminal that we provide. We recommend that if you use @(tb) heavily,
          you upload a public key, which will enable you to use a regular
          @(ssh) client.
      }
  }

  @clab-only{
      @instructionstep["Log in"
                       #:screenshot "log-in.png"]{
          You'll need an account to use @(tb). If you already have an
          account on @hyperlink["http://www.emulab.net"]{Emulab.net}, you may
          use that username and password. Or, if you have an account at the
          @hyperlink["http://portal.geni.net"]{GENI portal}, you may use the
          ``GENI User'' button to log in using that account.
          If not, you can apply to start a new
          project at @(url (apturl "signup.php")) and
          taking the "Start New Project" option. See the chapter about
          @seclink["users"]{@(tb) users} for more details about
          user accounts.
      }
  }

  @instructionstep["Select a profile"
                   #:screenshot "select-profile.png"]{
  Clicking the ``Change Profile'' button will let you select the
  @seclink["profiles"]{profile} that your @seclink["experiments"]{experiment}
  will be built from. A profile describes @seclink["rspecs"]{a set of
  resources} (both hardware and software) that will be used to start your
  experiment. On the hardware side, the profile will control whether you get
  @seclink["virtual-machines"]{virtual machines} or
  @seclink["physical-machines"]{physical ones}, how many there are, and what
  the network between them looks like. On the software side, the profile
  specifies the @seclink["disk-images"]{operating system and installed
  software}.

  Profiles come from two sources. Some of them are provided by @(tb) itself, and
  provide standard installation of popular operating systems, software stacks,
  etc. Others are @seclink["creating-profiles"]{created by other researchers}
  and may contain research software, artifacts and data used to gather published
  results, etc. Profiles represent a powerful way to enable
  @seclink["repeatable-research"]{repeatable research}.

  The large display in this dialog box shows the network topology of the
  profile, and a short description sits below the topology view.

  @apt-only{The @tt{OneVM} profile that we've selected here will get you a
        single VM running version 12.04 of the Ubuntu operating system---this
        is a good place to start.}
  @clab-only{The @tt{OpenStack} profile that we've selected here will give you
        a small OpenStack installation with one master node and one compute
        node. It provides a simple example of how complex software stacks can
        be packaged up within @(tb). If you'd prefer to start from bare metal,
        look for one of the profiles that installs a stock operating system on
        physical machines.}
  }

  @clab-only{
    @instructionstep["Pick a cluster"
                      #:screenshot "pick-datacenter.png"]{
      CloudLab can instantiate profiles on several different
      @seclink["hardware"]{backend clusters}. The cluster selector is located
      right above the ``Create'' button; the the cluster most suited to
      the profile you've chosen will be selected by default.
    } 
  }

  @instructionstep["Click Create!"
                   #:screenshot "please-wait.png"]{
  When you click the ``Create'' button, @(tb) will start preparing your experiment
  by selecting nodes, installing software, etc. as described in the profile.
  What's going on behind the scenes is that on one (or more) of the machines
  in one of the @seclink["hardware"]{@(tb) clusters}, a disk is being imaged,
  a @apt-vs-clab[#:apt "VM created and booted" #:clab "set of physical machines
  booted"], accounts created for you, etc.  This process usually takes a couple
  of minutes.

  @apt-only{If you have never used this email address with @(tb) before (or if
      you switch computers or browsers), @(tb) will send a verification email.
      Watch your email and enter the code into @(tb) when prompted. (If it
      doesn't arrive in a few minutes, check your spam folder!)"}
  }
  @instructionstep["Use your experiment"]{
  When your experiment is ready to use, the progress bar will be complete, and
  you'll be given a lot of new options at the bottom of the screen.

  @screenshot["node-list.png"]

  The ``Topology View'' shows the network topology of your experiment
  (which may be as simple as a single node). Clicking on a node in this
  view brings up a terminal in your browser that gives you a shell on the node.
  The ``List View'' lists all nodes in the topology, and in addition to 
  the in-browser shell, gives you the command to @(ssh) login to the node
  (if you provided a public key). The ``Manifest'' tab shows you the technical
  details of the resources allocated to your experiment. Any open terminals
  you have to the nodes show up as tabs on this page.

  Clicking on the ``Profile Instructions'' link (if present) will show
  instructions provided by the profile's creator regarding its use.

  Your experiment is yours alone, and you have full ``root'' access (via
  the @tt{sudo} command). No one else has access to the nodes in your
  experiment, and you may do anything at all inside of it, up to and including
  making radical changes to the operating system itself. We'll clean it all
  up when you're done!

  @clab-only{
    If you used our default OpenStack profile, the instructions will contain a
    link to the OpenStack web interface. The instructions will also give you
    a username and password to use.

    @screenshot["openstack-admin.png"]

    Since you gave @(tb) an @tt{ssh} public key as part of account creation,
    you can log in using the @tt{ssh} client on your laptop or desktop. The
    contoller node is a good place to start, since you can poke around with the
    OpenStack admin commands. Go to the "list view" on the experiment page to
    get a full command line for the @tt{ssh} command.

    @screenshot["openstack-shell.png"]

  }

  Your experiment will @bold{terminate automatically after a few
  hours.} When the experiment terminates, you will @bold{lose anything on
  disk} on the nodes, so be sure to copy off anything important early and
  often.  You can use the ``Extend'' button to submit a request to hold it
  longer, or the ``Terminate'' button to end it early.
  @apt-only{@seclink["registered-users"]{Registered users} get to hold their
      experiments for longer, so if you want to use @(tb) for much serious
      work, we recommend @seclink["register"]{registering for an account}.}
  }
]

@section{Next Steps}

@itemlist[
  @apt-only{@item{If you find @(tb) useful, @seclink["register"]{sign up} for
      an @seclink["users"]{account}---having one gives you access to more
      resources and lets you run longer experiments.}}

  @clab-only{@item{Read the @seclink["status-notes"]{notes on the status of
    @(tb)}}}

  @clab-only{@item{Try a profile that runs bare metal and set up a cloud stack
    yourself}}

  @item{Making your own profiles is easy: see the
  @seclink["creating-profiles"]{chapter on profile creation} for instructions.}

  @item{If you need help, or have questions or comments about @(tb)'s features,
  @seclink["getting-help"]{contact us}!}
]

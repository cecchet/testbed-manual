#lang scribble/manual
@(require racket/date)
@(require "defs.rkt")

@title[#:version apt-version
       #:date (date->string (current-date))]{The CloudLab Manual}

@author[
    "The CloudLab Team"
]

@bold{CloudLab is up and running, but in active development.
    For more information on the deployment schedule for the full version of
    CloudLab, see @hyperlink[(apturl)]{the CloudLab website}. Please also see
    the @seclink["status-notes"]{"Status Notes"} chapter of this document
    for notes regarding the status of the facility.}

@italic[(if (equal? doc-mode 'pdf)
    (list "The HTML version of this manual is available at " (hyperlink apt-doc-url apt-doc-url))
    (list "This manual is also available as a " (hyperlink "http://docs.cloudlab.us/manual.pdf" "PDF")))]


CloudLab is a "meta-cloud"---that is, it is not a cloud itself; rather, it is a
facility for building clouds. It provides bare-metal access and control over
a substantial set of computing, storage, and networking resources; on top of
this platform, users can install standard cloud software stacks, modify them,
or create entirely new ones.

The initial CloudLab deployment will consist of approximately 15,000 cores
distributed across three sites at the University of Wisconsin, Clemson
University, and the University of Utah. CloudLab interoperates with
existing testbeds including @hyperlink["http://www.geni.net"]{GENI} and
@hyperlink["http://www.emulab.net"]{Emulab}, to take advantage of
hardware at dozens of sites around the world.

The control software for CloudLab is
@hyperlink["https://gitlab.flux.utah.edu/emulab"]{open source}, and is built
on the foundation established for @hyperlink["http://www.emulab.net"]{Emulab},
@hyperlink["http://www.geni.net"]{GENI}, and
@hyperlink["http://www.aptlab.net"]{Apt}. Pointers to the details of this
control system can be found on CloudLab's @hyperlink[(apturl
"/technology.php")]{technology page}.

Take a look at the @seclink["status-notes"]{status notes}, and then
@seclink["getting-started"]{get started}!

@table-of-contents[]

@include-section["status-notes.scrbl"]
@include-section["getting-started.scrbl"]
@include-section["users.scrbl"]
@include-section["repeatable-research.scrbl"]
@include-section["creating-profiles.scrbl"]
@include-section["basic-concepts.scrbl"]
@include-section["advanced-topics.scrbl"]
@include-section["hardware.scrbl"]
@include-section["planned.scrbl"]
@include-section["cloudlab-tutorial.scrbl"]
@include-section["getting-help.scrbl"]

#lang scribble/manual
@(require racket/date)
@(require "defs.rkt")

@title[#:version apt-version
       #:date (date->string (current-date))]{The Apt Manual}

@author[
    "Robert Ricci" "Leigh Stoller" "Kirk Webb" "Jon Duerig" "Gary Wong" "Keith Downie" "Mike Hibler" "Eric Eide"
]

@italic[(if (equal? doc-mode 'pdf)
    (list "The HTML version of this manual is availble at " (hyperlink apt-doc-url apt-doc-url))
    (list "This manual is also available as a " (hyperlink "http://docs.aptlab.net/manual.pdf" "PDF")))]


Apt is a platform for sharing research; it is open to all researchers,
educators, and students. It is built around @seclink["profiles"]{profiles},
which describe @seclink["experiments"]{experiments}; when you instantiate a
profile, that specification is realized on one of @seclink["hardware"]{Apt's
clusters} using @seclink["virtual-machines"]{virtual} or
@seclink["physical-machines"]{physical} machines. The creator of a profile may
put code, data, and other resources into it, and the profile may consist of a
single machine or may describe an entire network.

Basic access to public profiles on Apt is provided
@seclink["guest-users"]{without the need to register} for an account---this
keeps the barriers to accessing research artifacts low. If you find the limited
resources that are provided to guest users valuable, you should
@seclink["register"]{sign up for a (free) account} to get access to more
resources and to @seclink["creating-profiles"]{create profiles of your own}.

Setting up the @bold{software} environment to run research artifacts is often
complicated, potentially requiring a specific version of an operating system,
dependencies on a large number of packages, and a complicated build and
configuration process for the research software.

Setting up the @bold{hardware} environment can be even more troublesome,
especially when one wants to reproduce published results, which may be highly
sensitive to the specific hardware they were gathered on. The problem becomes
complicated when more than one machine is needed to run the experiment. 

Apt's profiles capture this by describing both the software needed to run an
experiment and the hardware (physical or virtual) that the software needs to
run. By providing a hardware platform for running these profiles, Apt
essentially enables researchers to build their own testbed environments and
share them with others, without having to buy, build, or maintain the
underlying infrastructure.

Apt is built on @hyperlink["http://www.emulab.net/"]{Emulab} and
@hyperlink["http://www.geni.net/"]{GENI} technologies. It is built and operated
by the @hyperlink["http://www.flux.utah.edu"]{Flux Research Group}, part of the
@hyperlink["http://www.utah.edu/"]{University of Utah}'s
@hyperlink["http://www.cs.utah.edu/"]{School of Computing}. Apt is supported by
the National Science Foundation under award CNS-1338155 and by the University
of Utah. 

@table-of-contents[]

@include-section["getting-started.scrbl"]
@include-section["users.scrbl"]
@include-section["repeatable-research.scrbl"]
@include-section["creating-profiles.scrbl"]
@include-section["basic-concepts.scrbl"]
@include-section["advanced-topics.scrbl"]
@include-section["hardware.scrbl"]
@include-section["planned.scrbl"]
@include-section["getting-help.scrbl"]
